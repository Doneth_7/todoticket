﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Ticket.Models
{
    public class Promocion
    {
        [Key]
        public int idOferta { get; set; }
        [Required]
        public string decripcionOferta { get; set; }

        public int idEvento { get; set; }
        public Evento evento { get; set; }
    }
}