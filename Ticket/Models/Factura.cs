﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Ticket.Models
{
    public class Factura
    {
        [Key]
        public int idFactura { get; set; }
        [Required]
        public double Total { get; set; }

        public int idUsuario { get; set; }
        public Usuario usuario { get; set; }
    }
}