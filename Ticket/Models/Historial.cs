﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Ticket.Models
{
    public class Historial
    {
        [Key]
        public int idHistorial { get; set; }
        [Required]
        public string descripcion { get; set; }
        public int idUsuario { get; set; }
        public Usuario usuario { get; set; }
        public int idEvento { get; set; }
        public Evento evento { get; set; }
    }
}