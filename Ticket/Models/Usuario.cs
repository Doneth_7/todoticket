﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Ticket.Models
{
    public class Usuario
    {
        [Key]
        public int idUsuario { get; set; }
        [Required]
        public string nombre { get; set; }
        [Required]
        public string apellido { get; set; }
        [Required]
        public int dpi { get; set; }
        [Required]
        public string tarjeta { get; set; }
        [Required]
        public string correo { get; set; }
        [Required]
        public string nick { get; set; }
        [Required]
        public string contrasena { get; set; }
        public int idRol { get; set; }
        public Rol rol { get; set; }
    }
}