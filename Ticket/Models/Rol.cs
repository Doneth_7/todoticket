﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Ticket.Models
{
    public class Rol
    {
        [Key]
        public int idRol { get; set; }
        [Required]
        public string nombreRol { get; set; }
    }
}