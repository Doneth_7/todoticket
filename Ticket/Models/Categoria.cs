﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Ticket.Models
{
    public class Categoria
    {
        [Key]
        public int idCategoria { get; set; }
        [Required]
        public string nombreCategoria { get; set; }
    }
}