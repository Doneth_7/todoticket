﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Ticket.Models
{
    public class Boleto
    {
        [Key]
        public int idBoleto { get; set; }
        [Required]
        public string Disponibilidad { get; set; }
        [Required]
        public int idPromocion { get; set; }
        public Promocion promocion { get; set; }

        public int idEvento { get; set; }
        public Evento evento { get; set; }
    }
}