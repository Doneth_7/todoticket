﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Ticket.Models
{
    public class DetalleFactura
    {
        [Key]
        public int idDetalleFactura { get; set; }

        public int idEvento { get; set; }
        public Evento evento { get; set; }

        public int idFactura { get; set; }
        public Factura factura { get; set; }
    }
}