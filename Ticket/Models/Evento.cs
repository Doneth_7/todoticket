﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace Ticket.Models
{
    public class Evento
    {
        [Key]
        public int idEvento { get; set; }
        [Required]
        public string descripcion { get; set; }
        [Required]
        public string lugarEvento { get; set; }
        [Required]
        public string cupoEvento { get; set; }
        [Required]
        public string horaEvento { get; set; }
        [Required]
        public string fechaEvento { get; set; }
        [Required]
        public int precioEvento { get; set; }

        public int idCategoria { get; set; }
        public Categoria categoria { get; set; }

        public int idPersonal { get; set; }
        public Personal personal { get; set; }
    }
}