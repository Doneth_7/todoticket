﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Ticket.Models;

namespace Ticket.Controllers
{
    public class PromocionesController : ApiController
    {
        private TicketContext db = new TicketContext();

        // GET: api/Promociones
        public IQueryable<Promocion> GetPromocions()
        {
            return db.Promocions;
        }

        // GET: api/Promociones/5
        [ResponseType(typeof(Promocion))]
        public async Task<IHttpActionResult> GetPromocion(int id)
        {
            Promocion promocion = await db.Promocions.FindAsync(id);
            if (promocion == null)
            {
                return NotFound();
            }

            return Ok(promocion);
        }

        // PUT: api/Promociones/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPromocion(int id, Promocion promocion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != promocion.idOferta)
            {
                return BadRequest();
            }

            db.Entry(promocion).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PromocionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Promociones
        [ResponseType(typeof(Promocion))]
        public async Task<IHttpActionResult> PostPromocion(Promocion promocion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Promocions.Add(promocion);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = promocion.idOferta }, promocion);
        }

        // DELETE: api/Promociones/5
        [ResponseType(typeof(Promocion))]
        public async Task<IHttpActionResult> DeletePromocion(int id)
        {
            Promocion promocion = await db.Promocions.FindAsync(id);
            if (promocion == null)
            {
                return NotFound();
            }

            db.Promocions.Remove(promocion);
            await db.SaveChangesAsync();

            return Ok(promocion);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PromocionExists(int id)
        {
            return db.Promocions.Count(e => e.idOferta == id) > 0;
        }
    }
}