﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Ticket.Models;

namespace Ticket.Controllers
{
    public class HistorialesController : ApiController
    {
        private TicketContext db = new TicketContext();

        // GET: api/Historiales
        public IQueryable<Historial> GetHistorials()
        {
            return db.Historials;
        }

        // GET: api/Historiales/5
        [ResponseType(typeof(Historial))]
        public async Task<IHttpActionResult> GetHistorial(int id)
        {
            Historial historial = await db.Historials.FindAsync(id);
            if (historial == null)
            {
                return NotFound();
            }

            return Ok(historial);
        }

        // PUT: api/Historiales/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutHistorial(int id, Historial historial)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != historial.idHistorial)
            {
                return BadRequest();
            }

            db.Entry(historial).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HistorialExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Historiales
        [ResponseType(typeof(Historial))]
        public async Task<IHttpActionResult> PostHistorial(Historial historial)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Historials.Add(historial);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = historial.idHistorial }, historial);
        }

        // DELETE: api/Historiales/5
        [ResponseType(typeof(Historial))]
        public async Task<IHttpActionResult> DeleteHistorial(int id)
        {
            Historial historial = await db.Historials.FindAsync(id);
            if (historial == null)
            {
                return NotFound();
            }

            db.Historials.Remove(historial);
            await db.SaveChangesAsync();

            return Ok(historial);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HistorialExists(int id)
        {
            return db.Historials.Count(e => e.idHistorial == id) > 0;
        }
    }
}