﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ticket.Models;

namespace Ticket.Controllers
{
    public class EventosViewController : Controller
    {
        private TicketContext db = new TicketContext();

        // GET: EventosView
        public async Task<ActionResult> Index()
        {
            var eventoes = db.Eventoes.Include(e => e.categoria).Include(e => e.personal);
            return View(await eventoes.ToListAsync());
        }

        // GET: EventosView/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evento evento = await db.Eventoes.FindAsync(id);
            if (evento == null)
            {
                return HttpNotFound();
            }
            return View(evento);
        }

        // GET: EventosView/Create
        public ActionResult Create()
        {
            ViewBag.idCategoria = new SelectList(db.Categorias, "idCategoria", "nombreCategoria");
            ViewBag.idPersonal = new SelectList(db.Personals, "idPersonal", "nombre");
            return View();
        }

        // POST: EventosView/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "idEvento,descripcion,lugarEvento,cupoEvento,horaEvento,fechaEvento,precioEvento,idCategoria,idPersonal")] Evento evento)
        {
            if (ModelState.IsValid)
            {
                db.Eventoes.Add(evento);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.idCategoria = new SelectList(db.Categorias, "idCategoria", "nombreCategoria", evento.idCategoria);
            ViewBag.idPersonal = new SelectList(db.Personals, "idPersonal", "nombre", evento.idPersonal);
            return View(evento);
        }

        // GET: EventosView/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evento evento = await db.Eventoes.FindAsync(id);
            if (evento == null)
            {
                return HttpNotFound();
            }
            ViewBag.idCategoria = new SelectList(db.Categorias, "idCategoria", "nombreCategoria", evento.idCategoria);
            ViewBag.idPersonal = new SelectList(db.Personals, "idPersonal", "nombre", evento.idPersonal);
            return View(evento);
        }

        // POST: EventosView/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "idEvento,descripcion,lugarEvento,cupoEvento,horaEvento,fechaEvento,precioEvento,idCategoria,idPersonal")] Evento evento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(evento).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.idCategoria = new SelectList(db.Categorias, "idCategoria", "nombreCategoria", evento.idCategoria);
            ViewBag.idPersonal = new SelectList(db.Personals, "idPersonal", "nombre", evento.idPersonal);
            return View(evento);
        }

        // GET: EventosView/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evento evento = await db.Eventoes.FindAsync(id);
            if (evento == null)
            {
                return HttpNotFound();
            }
            return View(evento);
        }

        // POST: EventosView/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Evento evento = await db.Eventoes.FindAsync(id);
            db.Eventoes.Remove(evento);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
