﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Ticket.Models;

namespace Ticket.Controllers
{
    public class DetalleFacturasController : ApiController
    {
        private TicketContext db = new TicketContext();

        // GET: api/DetalleFacturas
        public IQueryable<DetalleFactura> GetDetalleFacturas()
        {
            return db.DetalleFacturas;
        }

        // GET: api/DetalleFacturas/5
        [ResponseType(typeof(DetalleFactura))]
        public async Task<IHttpActionResult> GetDetalleFactura(int id)
        {
            DetalleFactura detalleFactura = await db.DetalleFacturas.FindAsync(id);
            if (detalleFactura == null)
            {
                return NotFound();
            }

            return Ok(detalleFactura);
        }

        // PUT: api/DetalleFacturas/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDetalleFactura(int id, DetalleFactura detalleFactura)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != detalleFactura.idDetalleFactura)
            {
                return BadRequest();
            }

            db.Entry(detalleFactura).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DetalleFacturaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DetalleFacturas
        [ResponseType(typeof(DetalleFactura))]
        public async Task<IHttpActionResult> PostDetalleFactura(DetalleFactura detalleFactura)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DetalleFacturas.Add(detalleFactura);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = detalleFactura.idDetalleFactura }, detalleFactura);
        }

        // DELETE: api/DetalleFacturas/5
        [ResponseType(typeof(DetalleFactura))]
        public async Task<IHttpActionResult> DeleteDetalleFactura(int id)
        {
            DetalleFactura detalleFactura = await db.DetalleFacturas.FindAsync(id);
            if (detalleFactura == null)
            {
                return NotFound();
            }

            db.DetalleFacturas.Remove(detalleFactura);
            await db.SaveChangesAsync();

            return Ok(detalleFactura);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DetalleFacturaExists(int id)
        {
            return db.DetalleFacturas.Count(e => e.idDetalleFactura == id) > 0;
        }
    }
}