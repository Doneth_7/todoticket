namespace Ticket.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Ticket.Models.TicketContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Ticket.Models.TicketContext";
        }

        protected override void Seed(Ticket.Models.TicketContext context)
        {
            context.Rols.AddOrUpdate(new Models.Rol() { idRol = 1, nombreRol = "Administrador" });
            context.Rols.AddOrUpdate(new Models.Rol() { idRol = 2, nombreRol = "Cliente" });
            context.Usuarios.AddOrUpdate(new Models.Usuario() { idUsuario = 1, nombre = "Danna", apellido = "Vasquez", dpi = 1223034001, tarjeta = "credito", correo = "AnDaana@gmail.com", nick = "Dania", contrasena = "1234", rol = new Models.Rol() { idRol = 1, nombreRol = "Administrador" } });
            context.Categorias.AddOrUpdate(new Models.Categoria() { idCategoria = 1, nombreCategoria = "Concierto" });
            context.Personals.AddOrUpdate(new Models.Personal() { idPersonal = 1, nombre = "Brandon", apellido = "Barkler", edad = 29, numero = 22351050 });
            context.Eventoes.AddOrUpdate(new Models.Evento()
            {
                idEvento = 1,
                descripcion = "Una noche Epica con el primer concierto de la banda en el pais",
                lugarEvento = "Guatemala, zona 9 Parque de la industria",
                cupoEvento = "500",
                horaEvento = "19:30",
                fechaEvento = "2017/09/09",
                precioEvento = 500,
                categoria = new Models.Categoria() { idCategoria = 1, nombreCategoria = "Concierto" },
                personal = new Models.Personal() { idPersonal = 1, nombre = "Brandon", apellido = "Barkler", edad = 29, numero = 22351050 }
            });
        }
    }
}
