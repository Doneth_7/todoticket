namespace Ticket.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Boletoes",
                c => new
                    {
                        idBoleto = c.Int(nullable: false, identity: true),
                        Disponibilidad = c.String(nullable: false),
                        idPromocion = c.Int(nullable: false),
                        idEvento = c.Int(nullable: false),
                        promocion_idOferta = c.Int(),
                    })
                .PrimaryKey(t => t.idBoleto)
                .ForeignKey("dbo.Eventoes", t => t.idEvento, cascadeDelete: true)
                .ForeignKey("dbo.Promocions", t => t.promocion_idOferta)
                .Index(t => t.idEvento)
                .Index(t => t.promocion_idOferta);
            
            CreateTable(
                "dbo.Eventoes",
                c => new
                    {
                        idEvento = c.Int(nullable: false, identity: true),
                        descripcion = c.String(nullable: false),
                        lugarEvento = c.String(nullable: false),
                        cupoEvento = c.String(nullable: false),
                        horaEvento = c.String(nullable: false),
                        fechaEvento = c.String(nullable: false),
                        precioEvento = c.Int(nullable: false),
                        idCategoria = c.Int(nullable: false),
                        idPersonal = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idEvento)
                .ForeignKey("dbo.Categorias", t => t.idCategoria, cascadeDelete: true)
                .ForeignKey("dbo.Personals", t => t.idPersonal, cascadeDelete: true)
                .Index(t => t.idCategoria)
                .Index(t => t.idPersonal);
            
            CreateTable(
                "dbo.Categorias",
                c => new
                    {
                        idCategoria = c.Int(nullable: false, identity: true),
                        nombreCategoria = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.idCategoria);
            
            CreateTable(
                "dbo.Personals",
                c => new
                    {
                        idPersonal = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false),
                        apellido = c.String(nullable: false),
                        edad = c.Int(nullable: false),
                        numero = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idPersonal);
            
            CreateTable(
                "dbo.Promocions",
                c => new
                    {
                        idOferta = c.Int(nullable: false, identity: true),
                        decripcionOferta = c.String(nullable: false),
                        idEvento = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idOferta)
                .ForeignKey("dbo.Eventoes", t => t.idEvento, cascadeDelete: true)
                .Index(t => t.idEvento);
            
            CreateTable(
                "dbo.DetalleFacturas",
                c => new
                    {
                        idDetalleFactura = c.Int(nullable: false, identity: true),
                        idEvento = c.Int(nullable: false),
                        idFactura = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idDetalleFactura)
                .ForeignKey("dbo.Eventoes", t => t.idEvento, cascadeDelete: true)
                .ForeignKey("dbo.Facturas", t => t.idFactura, cascadeDelete: true)
                .Index(t => t.idEvento)
                .Index(t => t.idFactura);
            
            CreateTable(
                "dbo.Facturas",
                c => new
                    {
                        idFactura = c.Int(nullable: false, identity: true),
                        Total = c.Double(nullable: false),
                        idUsuario = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idFactura)
                .ForeignKey("dbo.Usuarios", t => t.idUsuario, cascadeDelete: true)
                .Index(t => t.idUsuario);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        idUsuario = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false),
                        apellido = c.String(nullable: false),
                        dpi = c.Int(nullable: false),
                        tarjeta = c.String(nullable: false),
                        correo = c.String(nullable: false),
                        nick = c.String(nullable: false),
                        contrasena = c.String(nullable: false),
                        idRol = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idUsuario)
                .ForeignKey("dbo.Rols", t => t.idRol, cascadeDelete: true)
                .Index(t => t.idRol);
            
            CreateTable(
                "dbo.Rols",
                c => new
                    {
                        idRol = c.Int(nullable: false, identity: true),
                        nombreRol = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.idRol);
            
            CreateTable(
                "dbo.Historials",
                c => new
                    {
                        idHistorial = c.Int(nullable: false, identity: true),
                        descripcion = c.String(nullable: false),
                        idUsuario = c.Int(nullable: false),
                        idEvento = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idHistorial)
                .ForeignKey("dbo.Eventoes", t => t.idEvento, cascadeDelete: true)
                .ForeignKey("dbo.Usuarios", t => t.idUsuario, cascadeDelete: true)
                .Index(t => t.idUsuario)
                .Index(t => t.idEvento);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Historials", "idUsuario", "dbo.Usuarios");
            DropForeignKey("dbo.Historials", "idEvento", "dbo.Eventoes");
            DropForeignKey("dbo.DetalleFacturas", "idFactura", "dbo.Facturas");
            DropForeignKey("dbo.Facturas", "idUsuario", "dbo.Usuarios");
            DropForeignKey("dbo.Usuarios", "idRol", "dbo.Rols");
            DropForeignKey("dbo.DetalleFacturas", "idEvento", "dbo.Eventoes");
            DropForeignKey("dbo.Boletoes", "promocion_idOferta", "dbo.Promocions");
            DropForeignKey("dbo.Promocions", "idEvento", "dbo.Eventoes");
            DropForeignKey("dbo.Boletoes", "idEvento", "dbo.Eventoes");
            DropForeignKey("dbo.Eventoes", "idPersonal", "dbo.Personals");
            DropForeignKey("dbo.Eventoes", "idCategoria", "dbo.Categorias");
            DropIndex("dbo.Historials", new[] { "idEvento" });
            DropIndex("dbo.Historials", new[] { "idUsuario" });
            DropIndex("dbo.Usuarios", new[] { "idRol" });
            DropIndex("dbo.Facturas", new[] { "idUsuario" });
            DropIndex("dbo.DetalleFacturas", new[] { "idFactura" });
            DropIndex("dbo.DetalleFacturas", new[] { "idEvento" });
            DropIndex("dbo.Promocions", new[] { "idEvento" });
            DropIndex("dbo.Eventoes", new[] { "idPersonal" });
            DropIndex("dbo.Eventoes", new[] { "idCategoria" });
            DropIndex("dbo.Boletoes", new[] { "promocion_idOferta" });
            DropIndex("dbo.Boletoes", new[] { "idEvento" });
            DropTable("dbo.Historials");
            DropTable("dbo.Rols");
            DropTable("dbo.Usuarios");
            DropTable("dbo.Facturas");
            DropTable("dbo.DetalleFacturas");
            DropTable("dbo.Promocions");
            DropTable("dbo.Personals");
            DropTable("dbo.Categorias");
            DropTable("dbo.Eventoes");
            DropTable("dbo.Boletoes");
        }
    }
}
