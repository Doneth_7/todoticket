// <auto-generated />
namespace Ticket.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Ininitial : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Ininitial));
        
        string IMigrationMetadata.Id
        {
            get { return "201705112057344_Ininitial"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
